package lesson1;

public class pva {
    public static void main(String[] args) {
        byte userValue1 = -128;
System.out.println(userValue1);
        short userValue2 = 32767;
System.out.println(userValue2);
        int userValue3 = 2147483647;
        System.out.println(userValue3);
        long userValue4 = 922337203;
        System.out.println(userValue4);
        float userValue5 = 1.24f;
        System.out.println(userValue5);
        double userValue6 = 1.7E+308;
        System.out.println(userValue6);
        String userValue7 = "Meow";
        System.out.println(userValue7);
        boolean userValue8 = false, userValue9 = true;
        System.out.println(userValue8);
        System.out.println(userValue9);
    }
}
